// import logo from "./logo.svg";
import "./App.css";
import Navigation from "./Components/Navigation";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import React, { useState, useMemo, useEffect } from "react";
import "./font/gt/stylesheet.css";

import {
  unstable_createMuiStrictModeTheme as createMuiTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
// import { purple } from "@material-ui/core/colors";

import Login from "./Pages/Login";
// import Home from "./Pages/Home/Home";
// import { Button } from "@material-ui/core";
import { UserContext } from "./Context/userContext";
import { ThemeContext } from "./Context/theme";
import News from "./Pages/News";
import Pages from "./Pages/Pages";
import { Questions } from "./Pages/Questions/Questions";
import Misc from "./Pages/Misc";
import Users from "./Pages/Users";
import Account from "./Pages/Account";
import { Products } from "./Pages/Products/Products";
import VendingMachines from "./Pages/VendingMachines";
import { Applications } from "./Pages/Applications/Applications";
import Home from "./Pages/Home/Home";

function App() {
  const [user, setUser] = useState({ authenticated: true });
  const [darkState, setDarkState] = useState(false);
  const palletType = darkState ? "dark" : "light";
  const darkTheme = createMuiTheme({
    palette: {
      primary: {
        // Purple and green play nicely together.
        dark: "1E1E24",
        main: "#deedee",
      },
      secondary: {
        // This is green.A700 as hex.
        main: "#11cb5f",
      },
      appBar: { color: "#9e9e9e" },
      type: palletType,
    },
  });
  const handleThemeChange = () => {
    setDarkState(!darkState);
  };

  const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  // const isAuthenticated = localStorage.getItem("authenticated");
  // console.log("user", userValue, isAuthenticated);
  // // const Pages = () => {};
  console.log("user first", value);
  const NavigationTheme = (props) => {
    return <Navigation theme={handleThemeChange}>{props.children}</Navigation>;
  };
  const getThemeCookie = async () => {
    // await console.log("ran");
    const theme = await localStorage.getItem("theme");
    // await console.log(theme);
    if (theme === "true") {
      // await console.log("set");
      setDarkState(true);
    } else {
      // await console.log("set");
      localStorage.setItem("theme", false);
      setDarkState(false);
    }
  };
  const getUserCookie = async () => {
    await console.log("User");
    const auth = await localStorage.getItem("authenticated");
    await console.log("User cookie is", value.user.authenticated);
    if (auth === "true") {
      await console.log("set");
      await setUser({ authenticated: true });
    } else {
      await console.log("set");
      await localStorage.setItem("authenticated", false);
      await setUser({ authenticated: false });
    }
  };
  const setTheme = async () => {
    await localStorage.setItem("theme", !darkState);
    handleThemeChange();
  };
  useEffect(() => {
    getThemeCookie();
    getUserCookie();
  }, []);
  return (
    <div>
      <Router>
        {/* <div className="App">
        <Navigation />
      </div> */}
        {/* <div>{value.user}hello</div> */}

        <ThemeProvider theme={darkTheme}>
          <ThemeContext.Provider value={{ darkState, setTheme }}>
            <UserContext.Provider value={value}>
              <Route
                exact
                path="/"
                component={() => {
                  return <a href="/dashboard">Admin</a>;
                }}
              />
              <Route
                exact
                path="/login"
                render={() => {
                  return value.user.authenticated ? (
                    <Redirect to="/dashboard" />
                  ) : (
                    <Redirect to="/login" />
                  );
                }}
              />{" "}
              {/* <Route
                path="/admin"
                render={({ match: { url } }) => (
                  <>
                    <Route
                      path={`${url}/`}
                      component={() => {
                        return <div>Backend</div>;
                      }}
                      exact
                    />
                    <Route
                      path={`${url}/home`}
                      component={() => {
                        return <div>Home</div>;
                      }}
                    />
                    <Route
                      path={`${url}/users`}
                      component={() => {
                        return <div>Users</div>;
                      }}
                    />
                  </>
                )}
              /> */}
              <Route
                path="/dashboard"
                render={({ match: { url } }) => {
                  console.log(url);
                  return value.user.authenticated ? (
                    <>
                      {/* <Redirect to="/dashboard" /> */}
                      <NavigationTheme>
                        <Route path={`${url}/`} component={Home} exact />
                        <Route path={`${url}/news`} component={News} />
                        <Route path={`${url}/content`} component={Pages} />
                        <Route path={`${url}/account`} component={Account} />
                        <Route path={`${url}/customers`} component={Users} />
                        <Route
                          path={`${url}/questions`}
                          component={Questions}
                        />
                        <Route path={`${url}/misc`} component={Misc} />

                        <Route path={`${url}/products`} component={Products} />
                        <Route
                          path={`${url}/machine`}
                          component={VendingMachines}
                        />
                        <Route path={`${url}/app`} component={Applications} />
                      </NavigationTheme>
                    </>
                  ) : (
                    <Redirect to="/login" />
                  );
                }}
              />
              <Route exact path="/login" component={Login} />
              {/* <Route
                exact
                path="/dashboard"
                component={() => {
                  return (
                    <NavigationTheme>
                      <Home />
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/content"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>Content</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/misc"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>Misc</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/news"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>News</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/questions"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>Questions</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/products"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>products</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/machine"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>Machine</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/app"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>App</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/account"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>Account</div>
                    </NavigationTheme>
                  );
                }}
              />
              <Route
                path="/dashboard/customers"
                component={() => {
                  return (
                    <NavigationTheme>
                      <div>customers</div>
                    </NavigationTheme>
                  );
                }}
              /> */}
            </UserContext.Provider>
          </ThemeContext.Provider>
        </ThemeProvider>
      </Router>
    </div>
  );
}

export default App;
