import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import { Typography, Divider } from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import CloseIcon from "@material-ui/icons/Close";

export default function BottomDrawerHalf(props) {
  const toggleDrawer = () => {
    props.callBack();
  };
  console.log(window.innerHeight);
  return (
    <div>
      <React.Fragment key={"bottom"}>
        <SwipeableDrawer
          anchor={"bottom"}
          open={props.state}
          onOpen={() => toggleDrawer()}
        >
          <div
            style={{
              display: "flex",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Typography style={{ padding: 15 }}>Бараа нэмэх</Typography>
            <Fab
              color="primary"
              aria-label="add"
              onClick={() => toggleDrawer()}
              style={{
                height: 36,
                width: 36,
                marginRight: 20,
                background:
                  "linear-gradient(45deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 67%, rgba(252,176,69,1) 100%)",
                color: "white",
              }}
            >
              <CloseIcon />
            </Fab>
          </div>
          <Divider />
          <div style={{ overflowY: "scroll" }}>{props.children}</div>
        </SwipeableDrawer>
      </React.Fragment>
    </div>
  );
}
