import React, { useState, useRef, useEffect } from "react";
import "./EditPage.scss";
import Wysiwyg from "../Wysiwyg";
import WysiwygViewer from "../WysiwygViewer";
import TextField from "@material-ui/core/TextField";

export const EditPage = () => {
  const ref = useRef(null);
  const ref1 = useRef(null);
  const [data, setdata] = useState("");
  const [Demo, showDemo] = useState(false);
  console.log(data);
  const dataSet = (data) => {
    setdata(data);
  };
  const handleResize = () => {
    if (ref.current) {
      ref1.current.textContent =
        "Энэхүү хуудас нь дор хаяж 1200px ээс дээр дэлгэцэн дээр компьютер дээр яаж харагдуулхыг үзүүлнэ . Одоогийн байдлаар дэлгэцийн өргөн" +
        "(" +
        ref1.current.offsetWidth +
        "px)" +
        "Дор хаяж энэ хэмжээ 600 аас дээш байх ёстой";
    }
    console.log(ref1.current);
  };
  useEffect(() => {
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  console.log(ref);
  handleResize();
  return (
    <div className="edit-wrapper">
      <div>
        <div style={{ margin: 5 }}>
          <TextField id="filled-basic" label="Гарчиг" variant="filled" />
          <TextField
            id="filled-basic"
            label="Бичсэн"
            variant="filled"
            style={{ marginLeft: 10 }}
          />
        </div>
        <div className="edit-title">
          Хуудсын контент.
          {data
            ? "Доор зайд өөрчлөлтөө хийнэ үү"
            : "Доор зайд хуудсын мэдээлэл оруулна уу"}
        </div>
        <div className="edit-content">
          <Wysiwyg data={data} callBack={dataSet} />{" "}
          <div
            className="edit-title"
            onClick={() => {
              showDemo(!Demo);
              handleResize();
            }}
          >
            {Demo
              ? "Энд дарж далд хийнэ үү"
              : "Утас болон компьютер дээр хэрхэн харуулах тохиргоо. Энд дарж харна уу"}
          </div>
        </div>

        {Demo ? (
          <div className="edit-demo">
            <div ref={ref} className=" edit edit-computer">
              <div className="edit-title" ref={ref1}>
                Энэхүү хуудас нь дор хаяж 1200px ээс дээр дэлгэцэн дээр
                компьютер дээр яаж харагдуулхыг үзүүлнэ.Контентийн одоогийн
                хэмжээ+{" "}
                {ref.current ? ":" : "(" + (window.innerWidth - 500) + "px)"}{" "}
                Өөр хэмжээ үзэх бол цонхыг томруулж жижигрүүлнэ үү
              </div>
              <WysiwygViewer data={data} />
            </div>
            <div className="edit edit-mobile">
              <div className="edit-title">
                Гар утсан дээр Энэ хэмжээ огт өөрчлөгдөхгүй
              </div>
              <WysiwygViewer data={data} />
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
