import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import DashboardIcon from "@material-ui/icons/Dashboard";
// import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
// import PeopleIcon from "@material-ui/icons/People";
// import BarChartIcon from "@material-ui/icons/BarChart";
// import LayersIcon from "@material-ui/icons/Layers";
import AssignmentIcon from "@material-ui/icons/Assignment";
import ShopIcon from "@material-ui/icons/Shop";
import PageviewIcon from "@material-ui/icons/Pageview";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import AirplayIcon from "@material-ui/icons/Airplay";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
// import AppsIcon from "@material-ui/icons/Apps";
import HomeIcon from "@material-ui/icons/Home";
// import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
// import { Typography } from "@material-ui/core";
// const useStyles = makeStyles((theme) => ({
//   listSubHeaderRoot: {
//     backgroundColor: theme.color.primary,
//     // color: "#252525",
//     /* To change the font, use the fontFamily rule */
//   },
// }));
export const WebControlItems = (
  <div>
    <ListSubheader color="secondary" inset disableSticky>
      Веб удирдлага
    </ListSubheader>{" "}
    <ListItem button component={Link} to="/dashboard/">
      <ListItemIcon>
        <HomeIcon />
      </ListItemIcon>
      <ListItemText primary="Нүүр" />
    </ListItem>
    <ListItem button component={Link} to="/dashboard/content">
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Хуудсууд" />
    </ListItem>
    <ListItem button component={Link} to="/dashboard/news">
      <ListItemIcon>
        <PageviewIcon />
      </ListItemIcon>
      <ListItemText primary="Мэдээ мэдээлэл" />
    </ListItem>
    <ListItem button component={Link} to="/dashboard/questions">
      <ListItemIcon>
        <QuestionAnswerIcon />
      </ListItemIcon>
      <ListItemText primary="Түгээмэл асуултууд" />
    </ListItem>
    {/* <ListItem button component={Link} to="/dashboard/misc">
      <ListItemIcon>
        <AppsIcon />
      </ListItemIcon>
      <ListItemText primary="Бусад" />
    </ListItem> */}
  </div>
);

export const VendingControlItems = (
  <div>
    <ListSubheader color="secondary" inset disableSticky>
      Vending удирдлага
    </ListSubheader>
    <ListItem button component={Link} to="/dashboard/products">
      <ListItemIcon>
        <AddShoppingCartIcon />
      </ListItemIcon>
      <ListItemText primary="Бараа" />
    </ListItem>
    <ListItem button component={Link} to="/dashboard/machine">
      <ListItemIcon>
        <ShopIcon />
      </ListItemIcon>
      <ListItemText primary="Vending Machine" />
    </ListItem>
    <ListItem button component={Link} to="/dashboard/app">
      <ListItemIcon>
        <AirplayIcon />
      </ListItemIcon>
      <ListItemText primary="Vending Application" />
    </ListItem>
  </div>
);
export const AccountControlItems = (
  <div>
    <ListSubheader color="secondary" inset disableSticky>
      <div>Account удирдлага</div>
    </ListSubheader>
    <ListItem button component={Link} to="/dashboard/account">
      <ListItemIcon>
        <AccountCircleIcon />
      </ListItemIcon>
      <ListItemText primary="Аккаунт тохиргоо" />
    </ListItem>
    <ListItem button component={Link} to="/dashboard/customers">
      <ListItemIcon>
        <SupervisorAccountIcon />
      </ListItemIcon>
      <ListItemText primary="Хэрэглэгчид" />
    </ListItem>
    {/* <ListItem button component={Link} to="/dashboard/employee">
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Ажилчид" />
    </ListItem> */}
  </div>
);
export const ChartItems = (
  <div>
    <ListSubheader color="secondary" inset disableSticky>
      Тайлан
    </ListSubheader>
    <ListItem button component={Link} to="/dashboard">
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Хугацаагаар" />
    </ListItem>
    <ListItem button component={Link} to="/123">
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Хэрэглэгчийн" />
    </ListItem>
    <ListItem button component={Link} to="/123">
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Машины" />
    </ListItem>
  </div>
);
export const Extra = (
  <div>
    <ListSubheader color="secondary" inset disableSticky>
      Нэмэлт
    </ListSubheader>
    <ListItem button component={Link} to="/123" disabled>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItem>
    <ListItem button component={Link} to="/123" disabled>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItem>
    <ListItem button component={Link} to="/123" disabled>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItem>
  </div>
);
