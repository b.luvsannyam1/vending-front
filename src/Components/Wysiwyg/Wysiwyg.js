// import React, { useState, useEffect } from "react";
// import "./Wysiwyg.scss";
// import CKEditor from "@ckeditor/ckeditor5-react";
// import InlineEditor from "@ckeditor/ckeditor5-build-inline";

// const Wysiwyg = (props) => {
//     class MyUploadAdapter {
//         constructor(loader) {
//             // The file loader instance to use during the upload.
//             console.log(loader);
//             this.loader = loader;
//         }

//         // Starts the upload process.
//         upload() {
//             return this.loader.file.then(
//                 (file) =>
//                     new Promise((resolve, reject) => {
//                         console.log("sad", file);
//                         this._initRequest();
//                         this._initListeners(resolve, reject, file);
//                         this._sendRequest(file);
//                     })
//             );
//         }

//         // Aborts the upload process.
//         abort() {
//             if (this.xhr) {
//                 this.xhr.abort();
//             }
//         }

//         _initRequest() {
//             const apiUrl = "http://localhost:5000/api/" + "content/upload";
//             const xhr = (this.xhr = new XMLHttpRequest());
//             xhr.open("POST", apiUrl, true);
//             xhr.responseType = "json";
//             let token = JSON.parse(localStorage.getItem("client_token"));
//             xhr.setRequestHeader("Authorization", "Bearer " + token);
//             xhr.setRequestHeader("Content-Type", "application/json");
//         }
//         _initListeners(resolve, reject, file) {
//             const xhr = this.xhr;
//             const loader = this.loader;
//             const genericErrorText = `Couldn't upload file: ${file.name}.`;

//             xhr.addEventListener("error", () => reject(genericErrorText));
//             xhr.addEventListener("abort", () => reject());
//             xhr.addEventListener("load", () => {
//                 const response = xhr.response;
//                 console.log(response);
//                 // This example assumes the XHR server's "response" object will come with
//                 // an "error" which has its own "message" that can be passed to reject()
//                 // in the upload promise.
//                 //
//                 // Your integration may handle upload errors in a different way so make sure
//                 // it is done properly. The reject() function must be called when the upload fails.
//                 if (response) {
//                     if (!response || response.error) {
//                         console.log(response);
//                         return reject(
//                             response && response.error
//                                 ? response.error.message
//                                 : genericErrorText
//                         );
//                     } else {
//                         console.log(response);
//                         resolve({
//                             default: response.url,
//                         });
//                     }
//                 }

//                 // If the upload is successful, resolve the upload promise with an object containing
//                 // at least the "default" URL, pointing to the image on the server.
//                 // This URL will be used to display the image in the content. Learn more in the
//                 // UploadAdapter#upload documentation.
//             });

//             // Upload progress when it is supported. The file loader has the #uploadTotal and #uploaded
//             // properties which are used e.g. to display the upload progress bar in the editor
//             // user interface.
//             if (xhr.upload) {
//                 console.log("uploading this image");
//                 xhr.upload.addEventListener("progress", (evt) => {
//                     if (evt.lengthComputable) {
//                         loader.uploadTotal = evt.total;
//                         loader.uploaded = evt.loaded;
//                     }
//                 });
//             }
//         }
//         _sendRequest(file) {
//             let reader = new FileReader();
//             const convertBase64 = (fl, rdr) => {
//                 rdr.readAsDataURL(fl);
//                 rdr.onerror = (error) => {
//                     console.log("Error: ", error);
//                 };
//             };
//             let base64Image = convertBase64(file, reader);
//             // Prepare the form data.
//             reader.onload = () => {
//                 base64Image = reader.result;
//                 let jsonfile = JSON.stringify({
//                     image: base64Image,
//                 });

//                 this.xhr.onload = function () {
//                     jsonfile = JSON.stringify({
//                         image: base64Image,
//                     });
//                     console.log("sending ", jsonfile);
//                 };
//                 this.xhr.send(jsonfile);
//             };
//         }
//     }
//     console.log(props);
//     const [data, setdata] = useState(props.data);
//     useEffect(() => {
//         props.callBack(data);
//     }, [data]);
//     return (
//         <div>
//             <div>
//                 <h2>CKEditor туршилт </h2>
//                 <CKEditor
//                     editor={InlineEditor}
//                     data={data}
//                     config={{
//                         image: {
//                             // You need to configure the image toolbar, too, so it uses the new style buttons.
//                             toolbar: [
//                                 "imageTextAlternative",
//                                 "|",
//                                 "imageStyle:alignLeft",
//                                 "imageStyle:full",
//                                 "imageStyle:alignRight",
//                             ],

//                             styles: [
//                                 // This option is equal to a situation where no style is applied.
//                                 "full",

//                                 // This represents an image aligned to the left.
//                                 "alignLeft",

//                                 // This represents an image aligned to the right.
//                                 "alignRight",
//                             ],
//                         },
//                     }}
//                     onInit={(editor) => {
//                         // Connect the upload adapter using code below
//                         editor.plugins.get(
//                             "FileRepository"
//                         ).createUploadAdapter = function (loader) {
//                             return new MyUploadAdapter(loader);
//                         };
//                         console.log("Editor is ready to use!", editor);
//                     }}
//                     onChange={(event, editor) => {
//                         const data = editor.getData();
//                         setdata(data);
//                     }}
//                     onBlur={(event, editor) => {
//                         console.log("Blur.", editor);
//                     }}
//                     onFocus={(event, editor) => {
//                         console.log("Focus.", editor);
//                     }}
//                 />
//             </div>
//         </div>
//     );
// };

// export { Wysiwyg };
import React from "react";
// , { useState, useEffect }
import "./Wysiwyg.scss";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-inline";
// import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
// import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
// import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";
// import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar";
// import ImageCaption from "@ckeditor/ckeditor5-image/src/imagecaption";
// import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle";

export const Wysiwyg = ({ data, callBack }) => {
  return (
    <div>
      <div style={{ background: "white", color: "black" }}>
        <CKEditor
          config={{
            image: {
              // Configure the available styles.
              styles: ["alignLeft", "alignCenter", "alignRight"],

              // Configure the available image resize options.
              resizeOptions: [
                {
                  name: "imageResize:original",
                  label: "Original",
                  value: null,
                },
                {
                  name: "imageResize:50",
                  label: "50%",
                  value: "50",
                },
                {
                  name: "imageResize:75",
                  label: "75%",
                  value: "75",
                },
              ],

              // You need to configure the image toolbar, too, so it shows the new style
              // buttons as well as the resize buttons.
              toolbar: [
                "imageStyle:alignLeft",
                "imageStyle:alignCenter",
                "imageStyle:alignRight",
                "|",
                "imageResize",
                "|",
                "imageTextAlternative",
              ],
            },
          }}
          editor={ClassicEditor}
          data={data}
          onReady={(editor) => {
            // You can store the "editor" and use when it is needed.
            console.log("Editor is ready to use!", editor);
          }}
          onChange={(event, editor) => {
            callBack(editor.getData());
            console.log({ event, editor, data });
          }}
          onBlur={(event, editor) => {
            console.log("Blur.", editor);
          }}
          onFocus={(event, editor) => {
            console.log("Focus.", editor);
          }}
        />
      </div>
    </div>
  );
};
