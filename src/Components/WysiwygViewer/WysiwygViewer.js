import React from "react";
import "./WysiwygViewer.scss";
const WysiwygViewer = (props) => {
    return (
        <div className="body-container">
            <div
                className="body"
                dangerouslySetInnerHTML={{ __html: props.data }}
            ></div>
        </div>
    );
};

export { WysiwygViewer };
