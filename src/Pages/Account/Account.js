import {
  Divider,
  Paper,
  Typography,
  Button,
  TextField,
} from "@material-ui/core";
import React from "react";
import "./Account.scss";
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from "@material-ui/icons/Save";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import ClearAllIcon from "@material-ui/icons/ClearAll";

export const Account = () => {
  return (
    <div className="account-page">
      <div>
        <Paper>
          <Typography style={{ paddingLeft: 20 }} variant="h6">
            Хэрэглэгчийн мэдээлэл
          </Typography>
          <Divider />
          <div className="account-detail">
            <div className="account-image">
              <img src="https://cdn.psychologytoday.com/sites/default/files/styles/image-article_inline_full/public/field_blog_entry_images/2018-09/shutterstock_648907024.jpg?itok=ji6Xj8tv" />
            </div>
            <div className="account-data">
              <div>Нэр : Goku</div>
              <div>Зэрэглэл : Admin</div>
              <div>Утас : 89000130</div>

              <div>Цахим шуудан : luuya_0409@yahoo.com</div>
            </div>
          </div>
        </Paper>
        <Paper>
          <Typography style={{ paddingLeft: 20, marginTop: 10 }} variant="h6">
            Засах
          </Typography>
          <Divider />
          <div className="account-editor">
            <div className="account-detail">
              <div className="image-edit">
                <div className="account-image">
                  <img src="https://cdn.psychologytoday.com/sites/default/files/styles/image-article_inline_full/public/field_blog_entry_images/2018-09/shutterstock_648907024.jpg?itok=ji6Xj8tv" />
                </div>
                <Button startIcon={<CloudUploadIcon />}>Зураг оруулах</Button>
              </div>
            </div>
            <div style={{ margin: "auto" }}>
              <div style={{ padding: 10 }}>
                <TextField label="Нэр" variant="outlined" fullWidth />
              </div>
              <div style={{ padding: 10 }}>
                <TextField label="Дугаар" variant="outlined" fullWidth />
              </div>
              <div style={{ padding: 10 }}>
                <TextField label="Эмайл" variant="outlined" fullWidth />
              </div>
              <div style={{ padding: 10 }}>
                <TextField
                  label="Нууц үг"
                  style={{ width: "50%" }}
                  variant="outlined"
                />
                <TextField
                  label="Нууц үг давталт"
                  style={{ width: "50%" }}
                  variant="outlined"
                />
              </div>
            </div>
            <div
              style={{
                padding: 10,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Button variant="outlined" startIcon={<SaveIcon />}>
                Хадгалах
              </Button>
              <Button variant="outlined" startIcon={<ClearAllIcon />}>
                Цэвэрлэх
              </Button>
            </div>
            <div style={{ padding: 10 }}>
              <Button variant="outlined" startIcon={<DeleteIcon />}>
                Account-aa устгах
              </Button>
            </div>
          </div>
        </Paper>
      </div>
    </div>
  );
};
