import { Paper, Typography } from "@material-ui/core";
import React from "react";

export const BoxData = ({ title, data, icon, back }) => {
  return (
    <Paper
      elevation={0}
      style={{
        background: back
          ? back
          : "linear-gradient(45deg, #7ec322 30%, #fdbb2d 90%)",
        height: 170,
        alignItems: "center",
        alignContent: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          flexDirection: "row",
        }}
      >
        <div>
          <Typography
            variant="h6"
            style={{ padding: 10, color: "white", fontSize: 16 }}
          >
            {title}
          </Typography>
        </div>
        <div style={{ padding: 10, color: "white" }}>{icon}</div>
      </div>
      <Typography variant="h2" style={{ padding: 10, color: "white" }}>
        {data}
      </Typography>
    </Paper>
  );
};
