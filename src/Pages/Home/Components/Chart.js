import React from "react";
import { useTheme } from "@material-ui/core/styles";
import ChartistGraph from "react-chartist";
import Title from "./Title";
import "./Chart.scss";

export default function Chart() {
  const theme = useTheme();
  console.log(theme);
  var data = {
    labels: [
      "W1",
      "W2",
      "W3",
      "W4",
      "W5",
      "W6",
      "W7",
      "W8",
      "W9",
      "W10",
      "W10",
    ],
    series: [[1, 2, 4, 8, 6, -2, -1, -4, -6, -2]],
  };

  var options = {
    high: 10,
    low: -10,
    donut: true,
    axisX: {
      labelInterpolationFnc: function (value, index) {
        return index % 2 === 0 ? value : null;
      },
    },
  };

  var type = "Bar";

  return (
    <div
      style={{
        padding: 20,
      }}
    >
      <Title color="secondary">Today</Title>
      <div style={{ overflowX: "scroll" }}>
        <ChartistGraph data={data} options={options} type={type} />
      </div>
    </div>
  );
}
