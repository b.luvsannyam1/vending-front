import { Divider, Paper, Typography } from "@material-ui/core";
import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Chart from "./Components/Chart";
import Title from "./Components/Title";
import AssessmentIcon from "@material-ui/icons/Assessment";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import "./Home.scss";
import PieChart from "./Components/PieChart";
import BugTable from "./Components/BugTable";
import { BoxData } from "./Components/BoxData";
import ShowChartIcon from "@material-ui/icons/ShowChart";
import OpenInBrowserIcon from "@material-ui/icons/OpenInBrowser";
import GroupOutlinedIcon from "@material-ui/icons/GroupOutlined";
import PeopleAltRoundedIcon from "@material-ui/icons/PeopleAltRounded";
export default function Home() {
  const useStyles = makeStyles((theme) => ({
    paper: {
      padding: theme.spacing(2),
      display: "flex",
      overflow: "auto",
      flexDirection: "column",
    },
    fixedHeight: {
      height: 240,
    },
  }));
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  return (
    <div className="home">
      <Paper style={{ marginBottom: 10, display: "flex" }}>
        <AssessmentIcon style={{ height: 32, marginRight: 20 }} />
        <Typography component="h2" variant="h6">
          Ерөнхий мэдээллүүд
        </Typography>
      </Paper>
      <div className="home-grid">
        <div>
          <BoxData
            title="Веб рүү орсон хүний тоо(Сүүлийн сар)"
            data="0"
            icon={<GroupOutlinedIcon />}
            back="linear-gradient(to right, #8360c3, #2ebf91)"
          />
        </div>
        <div>
          <BoxData
            title="Олсон орлого(Энэ сар)"
            data="0"
            icon={<ShowChartIcon />}
            back="linear-gradient(to right, #11998e, #38ef7d)"
          />
        </div>{" "}
        <div>
          <BoxData
            title="Хэрэглэгчдийн тоо (Vending Machine)"
            data="0"
            icon={<PeopleAltRoundedIcon />}
            back="linear-gradient(to right, #642b73, #c6426e)"
          />
        </div>
        <div>
          <BoxData
            title="Хэрэглэгчдийн тоо (Upower)"
            data="0"
            icon={<OpenInBrowserIcon />}
            back="linear-gradient(to right, #aa4b6b, #6b6b83, #3b8d99)"
          />
        </div>
      </div>
      <Paper style={{ marginBottom: 10, display: "flex" }}>
        <TrendingUpIcon style={{ height: 32, marginRight: 20 }} />
        <Typography component="h2" variant="h6">
          Ерөнхий диаграммууд
        </Typography>
      </Paper>
      <div className="chart-grid">
        <div>
          <Paper style={{ height: "100%" }}>
            <div style={{ marginBottom: 10, display: "flex" }}>
              <TrendingUpIcon style={{ height: 32, marginRight: 20 }} />
              <Typography component="h2" variant="h6">
                Худалдан авалт өдрийн цагаар
              </Typography>
            </div>
            <Chart />
            Граф material ui тай ажиллахгүй байгаа тул одоогийн байдлаар
            ажиллахгүй байгаа
          </Paper>
        </div>
        <div>
          <Paper style={{ height: "100%" }}>
            <div style={{ marginBottom: 10, display: "flex" }}>
              <TrendingUpIcon style={{ height: 32, marginRight: 20 }} />
              <Typography component="h2" variant="h6">
                Хамгийн эрэлттэй бараа
              </Typography>
            </div>
            Граф material ui тай ажиллахгүй байгаа тул одоогийн байдлаар
            ажиллахгүй байгаа
          </Paper>
        </div>
      </div>
      <Paper style={{ marginBottom: 10, display: "flex" }}>
        <FormatListBulletedIcon style={{ height: 32, marginRight: 20 }} />
        <Typography component="h2" variant="h6">
          Бусад мэдээллүүд{" "}
        </Typography>
      </Paper>
      <div className="home-grid">
        <div>
          <Paper>
            <Typography style={{ padding: 10 }}>
              Хамгийн сүүлийн хийсэн худалдан авалт
            </Typography>{" "}
            <Divider />
            <BugTable />
          </Paper>
        </div>
        <div>
          <Paper>
            <Typography style={{ padding: 10 }}>Алдаанууд</Typography>{" "}
            <Divider />
            <BugTable />
          </Paper>
        </div>
      </div>
    </div>
  );
}
