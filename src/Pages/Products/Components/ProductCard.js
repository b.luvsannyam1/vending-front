import { Button, Paper, Typography } from "@material-ui/core";
import React from "react";

export const ProductCard = () => {
  return (
    <div>
      <Paper style={{ overflow: "hidden" }}>
        <div style={{ width: "100%" }}>
          <img
            style={{ width: "100%", height: "100%" }}
            src="https://assets.sainsburys-groceries.co.uk/gol/7900749/1/300x300.jpg"
          />
        </div>
        <div style={{ paddingLeft: 10 }}>
          <Typography>id : 1200</Typography>
          <Typography>Нэр : Кола</Typography>
          <Typography>Үнэ : 1200</Typography>
        </div>
        <div
          style={{
            display: "flex",
            margin: 5,
            justifyContent: "space-between",
          }}
        >
          <Button variant="outlined">Засах</Button>
          <Button variant="outlined">Устгах</Button>
        </div>
      </Paper>
    </div>
  );
};
