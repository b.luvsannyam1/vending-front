import React, { useState } from "react";
import { Typography, Paper, Button } from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import BottomDrawerHalf from "../../Components/BottomDrawerHalf/BottomDrawerHalf";
import AddProduct from "./Components/AddProduct";
import "./Products.scss";
import { ProductCard } from "./Components/ProductCard";

export const Products = () => {
  const [bottomState, setBottomState] = useState(false);
  const toggleBottomDrawer = () => {
    setBottomState(!bottomState);
  };
  return (
    <div>
      <Paper
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography style={{ padding: 5 }}>Бараанууд</Typography>
        <div style={{ paddingRight: 10 }}>
          <Fab
            color="primary"
            aria-label="add"
            style={{
              height: 36,
              width: 36,
              color: "white",
              margin: 5,
              background: "linear-gradient(to right, #283c86, #45a247)",
            }}
            onClick={toggleBottomDrawer}
          >
            <AddIcon />
          </Fab>
        </div>
      </Paper>
      <div className="products-grid">
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
      </div>
      <BottomDrawerHalf
        callBack={() => {
          toggleBottomDrawer();
        }}
        state={bottomState}
      >
        {/* <AddProduct /> */}
        Энд Бараа нэмэх хэсэг орно
      </BottomDrawerHalf>
    </div>
  );
};
