import * as React from "react";
import { DataGrid } from "@material-ui/data-grid";

export default function ContentTable() {
  return (
    <div style={{ height: 650, width: "100%" }}>
      <DataGrid
        columns={[
          { field: "id" },
          { field: "username" },
          { field: "age" },
          { field: "id" },
          { field: "username" },
          { field: "age" },
          { field: "id" },
          { field: "username" },
          { field: "age" },
        ]}
        rows={[
          {
            id: 1,
            username: "defunkt",
            age: 38,
          },
          {
            id: 2,
            username: "defunkt",
            age: 38,
          },
          {
            id: 3,
            username: "defunkt",
            age: 38,
          },
          {
            id: 4,
            username: "defunkt",
            age: 38,
          },
          {
            id: 4,
            username: "defunkt",
            age: 38,
          },
        ]}
        checkboxSelection
        onSelectionChange={(data) => console.log(data)}
      />
    </div>
  );
}
