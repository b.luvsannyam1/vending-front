import { Button, Paper } from "@material-ui/core";
import React, { useState } from "react";
import "./Questions.scss";
import { Typography } from "@material-ui/core";
import ContentTable from "./Components/Table";
import BottomDrawer from "../../Components/BottomDrawer/BottomDrawer";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import EditPage from "../../Components/EditPage";
// import { green } from "@material-ui/colors";

export function Questions(props) {
  const [bottomState, setBottomState] = useState(false);
  const toggleBottomDrawer = () => {
    setBottomState(!bottomState);
  };
  return (
    <div style={{ width: "100%", justifyContent: "center", display: "flex" }}>
      <Paper style={{ width: (window.innerWidth - 100) / 1.206451612903226 }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Typography style={{ padding: 20 }}>Хуудсууд</Typography>
          <div
            style={{
              margin: 15,
            }}
          >
            <Fab
              color="primary"
              aria-label="add"
              style={{
                height: 36,
                width: 36,
                background: "linear-gradient(45deg, #7ec322 30%, #fdbb2d 90%)",
                color: "white",
                marginRight: 5,
              }}
              onClick={toggleBottomDrawer}
            >
              <EditIcon />
            </Fab>
            <Fab
              color="primary"
              aria-label="add"
              style={{
                height: 36,
                width: 36,
                background:
                  "linear-gradient(45deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 67%, rgba(252,176,69,1) 100%)",
                color: "white",
              }}
              onClick={toggleBottomDrawer}
            >
              <DeleteIcon />
            </Fab>{" "}
            <Fab
              color="primary"
              aria-label="add"
              style={{
                height: 36,
                width: 36,
                background: "linear-gradient(45deg, #24f004 30%, #00ffda 90%)",
                color: "white",
              }}
              onClick={toggleBottomDrawer}
            >
              <AddIcon />
            </Fab>
          </div>
        </div>
        <div style={{ overflowX: "scroll" }}>
          <ContentTable />
        </div>
        <BottomDrawer
          callBack={() => {
            toggleBottomDrawer();
          }}
          state={bottomState}
        >
          <EditPage />
        </BottomDrawer>
      </Paper>
    </div>
  );
}
