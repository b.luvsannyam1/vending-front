import { Paper, Typography } from "@material-ui/core";
import React from "react";
import "./VendingMachines.scss";

const VendingActivity = () => {
  return (
    <div>
      <Paper>
        <Typography>
          Vending машины id :{Math.floor(Math.random() * 1000000000)}
        </Typography>
        <Typography>Идэвхтэй байгаа эсэх : Тийм</Typography>
      </Paper>
    </div>
  );
};

export const VendingMachines = () => {
  return (
    <div className="vending-grid">
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
      <VendingActivity />
    </div>
  );
};
